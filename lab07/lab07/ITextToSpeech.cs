﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab07
{
    public interface ITextToSpeech
    {
        void Speak(string text);
    }
}
