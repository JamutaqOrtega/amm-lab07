﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing.Net.Mobile.Forms;

namespace lab07
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            tts.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new TextToSpeechDemo());
            };

            battery.Clicked += async (sender, e) =>
            {
                await Navigation.PushAsync(new BatteryDemo());
            };

            qr.Clicked += async (sender, e) =>
            {
                try
                {
                    var scanner = new ZXing.Mobile.MobileBarcodeScanner();
                    scanner.TopText = "Escaner QR";
                    scanner.BottomText = "Escaneando...";

                    var result = await scanner.Scan();
                    if (result != null)
                    {
                        txtBarcode.Text = result.Text;
                    }
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Error", ex.Message.ToString(), "Ok");
                }
            };
        }
    }
}
